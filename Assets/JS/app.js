const accordion = document.querySelectorAll('.accordion');
for (let el of accordion) {
    el.addEventListener("click", function() {
        el.classList.toggle("active");
        el.nextElementSibling.classList.toggle("option-tog");
    });
}

const navToggle = document.querySelector('.toggle');
const navItems = document.querySelectorAll('.nav-item');
const badge = document.querySelectorAll('.badge')
const grid = document.querySelector('.grid');
const li = document.querySelectorAll('.nav-hover')
navToggle.addEventListener('click', function() {
    grid.classList.toggle('grid-max');
    for (nav of navItems) {
        nav.classList.toggle('nav-show');
    }
    for (b of badge) {
        b.classList.toggle('badge-hide');
    }
    for (el of li) {
        el.classList.toggle('nav-hover');
    }

})